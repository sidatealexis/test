DROP DATABASE PSI;
CREATE DATABASE PSI;
USE PSI;


CREATE TABLE Statut
(
    id_statut INT NOT NULL AUTO_INCREMENT,
    libelle_statut VARCHAR(50),
    PRIMARY KEY (id_statut)
);

CREATE TABLE Individu
(
    id INT NOT NULL AUTO_INCREMENT,
    nom VARCHAR(50),
    prenom VARCHAR(50),
    email VARCHAR(50),
    num INT,
    annuaire INT,
    statut INT,
    PRIMARY KEY (id),
    CONSTRAINT FK_statut FOREIGN KEY (statut) REFERENCES Statut(id_statut)
);

CREATE TABLE Groupe
(
    id_groupe INT NOT NULL AUTO_INCREMENT,
    libelle_groupe VARCHAR(50),
    annee_scolaire INT,
    PRIMARY KEY (id_groupe)
);

CREATE TABLE individu_groupe
(
    id_individu_groupe INT NOT NULL AUTO_INCREMENT,
    id_groupe INT NOT NULL,
    id_individu INT NOT NULL,
    PRIMARY KEY (id_individu_groupe),
    CONSTRAINT FK_individu FOREIGN KEY (id_individu) REFERENCES Individu(id),
    CONSTRAINT FK_groupe FOREIGN KEY (id_groupe) REFERENCES Groupe(id_groupe)
);

INSERT INTO Statut VALUES ("1","ETU");